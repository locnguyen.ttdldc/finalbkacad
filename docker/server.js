'use strict';

const express = require('express');

// Constants
const HOST = '0.0.0.0';
var port = normalizePort(process.env.APP_PORT || '3000');
var classvarible = process.env.CLASS;
var environment = process.env.APP_ENV;

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(port, HOST, () => {
  console.log(`Running on http://${HOST}`);
});

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
